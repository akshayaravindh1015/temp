import { Component } from '@angular/core';
import { RiskItem } from './risk-card/risk-card.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  public riskData: RiskItem[] = [
    {
      orderId: 1223,
      risk: 'This is a risk',
      date: '2021-07-07',
    },
    {
      orderId: 144223,
      risk: 'This is a risk',
      date: '2021-09-12',
    },
    {
      orderId: 15566223,
      risk: 'This is a risk',
      date: '2021-07-09',
    },
    {
      orderId: 126445423,
      risk: 'This is a risk',
      date: '2021-07-09',
    },
  ];

  title = 'temp';

  addNewRisk() {
    let date = new Date();
    this.riskData.push({
      orderId: 123,
      risk: 'New Risk',
      date: `${date.getDate()}-${date.getMonth()}-` + `${date.getFullYear()}`,
    })
  }

  riskValuesChanged(event: RiskItem, index: number) {
    this.riskData[index] = event;
    console.log("Entire Array - Updated in parent component : ")
    console.log(this.riskData);
  }
}