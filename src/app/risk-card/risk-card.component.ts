import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { RiskItem } from './risk-card.model';

@Component({
  selector: 'risk-card',
  templateUrl: 'risk-card.component.html',
  styleUrls: ['risk-card.component.scss'],
})
export class RiskCardComponent implements OnInit {

  @Input() riskItem!: RiskItem;
  @Output() riskValuesChanged: EventEmitter<RiskItem> = new EventEmitter();

  riskTitle: string = '';
  orderId: number = 0;
  date: Date = new Date();
  isRiskTitleInEditState: boolean = false;
  isRiskDateInEditState: boolean = false;
  isOrderIdInEditState: boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.riskTitle = this.riskItem.risk;
    this.orderId = this.riskItem.orderId;
    let tempDate = new Date();
    tempDate.setFullYear(+this.riskItem.date.substring(0, 4));
    tempDate.setMonth(+this.riskItem.date.substring(5, 7) - 1);
    tempDate.setDate(+this.riskItem.date.substring(8, 10));
    this.date = tempDate;
  }

  editRiskTitle(event: Event) {
    event.stopPropagation();
    this.isRiskTitleInEditState = true;
  }
  editRiskDate(event: Event) {
    event.stopPropagation();
    this.isRiskDateInEditState = true;
  }
  editDate(event: Event) {
    event.stopPropagation();
    this.isRiskDateInEditState = true;
  }
  editOrderId(event: Event) {
    event.stopPropagation();
    this.isOrderIdInEditState = true;
  }

  saveData() {
    this.isRiskTitleInEditState = false;
    this.isOrderIdInEditState = false;
    this.isRiskDateInEditState = false;
    this.riskValuesChanged.emit(
      new RiskItem(
        this.orderId,
        this.riskTitle,
        this.getDateFormatFromDate(),
      )
    );
  }

  getDateFormatFromDate(): string {
    if (typeof this.date == 'string') {
      return this.date;
    }
    return this.date.getFullYear() + '-' +
      (this.date.getMonth() < 10 ? '0' : '') + this.date.getMonth() + '-' +
      (this.date.getDate() < 10 ? '0' : '') + this.date.getDate()
  }

}
