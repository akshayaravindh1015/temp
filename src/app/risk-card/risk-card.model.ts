export class RiskItem {
    orderId!: number;
    risk!: string;
    date!: string;

    constructor(id: number, risk: string, date: string) {
        this.orderId = id;
        this.risk = risk;
        this.date = date;
    }
}